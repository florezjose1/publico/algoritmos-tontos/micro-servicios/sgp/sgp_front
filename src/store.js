import { createStore } from 'vuex'

// Create a new store instance.
export const store = createStore({
    state: {
        auth: false,
        authentication: null,
        profile: {
            username: "",
            role: ""
        },
        admin: ["admin"],
        evaluator: ["evaluator"],
        projects: null,
        categories: null
    },
    mutations: {
        logout: (state) => state.auth = false,
        login: (state) => state.auth = true
    }
});
