import { createRouter, createWebHistory } from 'vue-router'
import Admin from '../views/Admin.vue'

import {store} from "./../store";
import Projects from "@/views/admin/Projects.vue";
import Login from "@/views/auth/Login.vue";
import Register from "@/views/auth/Register.vue";



const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register.vue',
      component: Register
    },
    {
      beforeEnter: ((to, from, next) => {
        console.log('Access');
        next(store.state.auth);
      }),
      path: '/admin',
      // name: 'Admin',
      component: Admin,
      children: [
        {
          path: '/',
          name: 'Project',
          component: Projects
        }
      ]
    }

  ]
})

export default router
